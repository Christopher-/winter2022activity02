public class Application {
	public static void main (String[] args) {
		Tiger[] animal = new Tiger[1];
		Tiger[] streak = new Tiger[3];
		animal[0] = new Tiger();
		streak[0] = new Tiger();
		streak[2] = new Tiger();
		animal[0].name = "Tiger";
		animal[0].lifespan = 10;
		animal[0].species = "Panthera Tigris";
		animal[0].percent = 54.3;
		System.out.println(animal[0].name);
		System.out.println(animal[0].lifespan);
		System.out.println(animal[0].species);
		System.out.println(animal[0].percent);
		animal[0].hunt();
		animal[0].roam();
		streak[0].name = "streak";
		streak[2].lifespan = 12;
		System.out.println(streak[0].name);
		System.out.println(streak[2].lifespan);
	}	
}