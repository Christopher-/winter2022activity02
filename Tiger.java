public class Tiger {
	public String name;
	public int lifespan;
	public String species;
	public double percent;
	public void hunt() {
		System.out.println("I hunt deers");
	}
	public void roam() {
		System.out.println("Moving to the forest");
	}		
}